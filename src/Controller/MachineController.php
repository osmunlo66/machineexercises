<?php
namespace App\Controller;

use App\Repository\MachineRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MachineController
 *
 * @package App\Controller
 *         
 * @Route(path="/machinesapi/")
 */
class MachineController
{

    private $repository;

    public function __construct(MachineRepository $machineRep)
    {
        $this->repository = $machineRep;
    }

    /**
     *
     * @Route("addProduct",name="add_new_machine",methods={"POST"})
     */
    public function addProduct(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        
        $UID = $data['uid'];
        $brand = $data['brand'];
        $model = $data['model'];
        $manufacturer = $data['manufacturer'];
        $price = $data['price'];
        $images = $data['images'];

        if (empty($UID) || empty($brand) || empty($model) || empty($manufacturer) || empty($price)) {
            throw new NotFoundHttpException('Missing essential attributes: Either UID, brand, model, manufacturer or price has not been set up');
        }

        $this->repository->saveMachine($UID, $brand, $model, $manufacturer, $price, $images);

        return new JsonResponse([
            'status' => 'New machine added'
        ],Response::HTTP_CREATED);
    }

    /**
     * @Route("getProductByUid/{uid}",name="find_by_UID",methods={"GET"})
     */
    public function getProductByUid($uid): JsonResponse
    {
        $machine = $this->repository->findOneBy([
            'UID' => $uid
        ]);
        if (empty($machine)) {
            return new JsonResponse([
                'status' => 'Database error: Resource not found'
            ], Response::HTTP_NOT_FOUND);
        }
        $data = [
            'uid' => $machine->getUID(),
            'brand' => $machine->getBrand(),
            'model' => $machine->getModel(),
            'manufacturer'=>$machine->getManufacturer(),
            'price'=>$machine->getPrice(),
            'images'=>$machine->getImages()
        ];
        
        return new JsonResponse($data,Response::HTTP_OK);
    }
    
    /**
     * @Route("listProducts",name="list_products",methods={"GET"})
     */
    public function listProducts():JsonResponse{
        $machines = $this->repository->findAll();
        
        foreach ($machines as $machine){
            $data[]=[
                'uid' => $machine->getUID(),
                'brand' => $machine->getBrand(),
                'model' => $machine->getModel(),
                'manufacturer'=>$machine->getManufacturer(),
                'price'=>$machine->getPrice(),
                'images'=>$machine->getImages()
            ];
        }
        
        return new JsonResponse($data, Response::HTTP_OK);
    }
    
    /**
     * @Route("updateProduct/{uid}",name="update_product",methods={"PUT"})
     */
    public function updateProduct($uid, Request $request) {
        $machine = $this->repository->findOneBy([
            'UID' =>$uid
        ]);
        $data = json_decode($request->getContent(),true);
        
        empty($data['brand'])?true:$machine->setBrand($data['brand']);
        empty($data['model'])?true:$machine->setModel($data['model']);
        empty($data['manufacturer'])?true:$machine->setManufacturer($data['manufacturer']);
        empty($data['price'])?true:$machine->setPrice($data['price']);
        empty($data['images'])?true:$machine->setImages($data['images']);
        
        $this->repository->updateMachine($machine);
        
        return new JsonResponse([
           'status'=>'Product updated' 
        ], Response::HTTP_OK);
    }
    
    /**
     * @Route("deleteProduct/{uid}",name="delete_product",methods={"DELETE"})
     */  
    public function delete($uid):JsonResponse{
        $machine = $this->repository->findOneBy([
            'UID'=> $uid
        ]);
        
        if (empty($machine)) {
            return new JsonResponse([
                'status' => 'Database error: Resource not found'
            ], Response::HTTP_NOT_FOUND);
        }
        
        $this->repository->removeMachine($machine);
        
        return new JsonResponse([
            'status'=>'Product deleted'
        ]);
    }
}

?>