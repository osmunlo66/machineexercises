MACHINE PRODUCTS ACCESS API

Symfony 5 Required to launch.

STEPS TO LAUNCH:

1. Enter the project folder
2. Open the .env file with a text editor
3. In the "DATABASE_URL" field, add any MySQL database following this structure:
	username:password@address:port/database_name?serverVersion=5.7

4. Enter the project folder via CMD
5. Run ~symfony serve (localhost link provided within the command line once server is running)
6. Use link to make any requests below



ENDPOINTS:

*---- /machinesapi/listProducts (METHOD: GET)

Lists all the Machine objects present in the Database.

No additional params required.

Returns: JSON


*---- /machinesapi/getProductByUid/{uid} (METHOD: GET)

Finds Machine object by UID (ID is a duplicate field, so I couldn't use it as shown in the example)

Params:

uid: String containing the uid

Returns: JSON

*---- /machinesapi/addProduct (METHOD: POST)

Adds a product to the database. Needs a request body.

Request body:
-----------------------------
{
  "uid": String,
  "brand":String,
  "model":String,
  "manufacturer":String,
  "price": Float,
  "images" : JSON (optional)
}
-----------------------------

Example JSON of "images" field:
-----------------------------
[
{
"id" : "072823c0-b042-4ccf-9162-43114d802b76",
"type" : "thumbnail",
"url" :
"https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Fully_automated_schiffli_embroidery_machine_by_Saurer.jpg/1024px-Fully_automated_schiffli_embroidery_machine_by_Saurer.jpg"
}]
-----------------------------

Returns: JSON (HTTP response)

*---- /machinesapi/updateProduct/{uid} (METHOD: PUT)

Updates the fields that the user wants to change. Needs request body

Params:

uid: String containing the uid of the object to modify.

Request body:
-----------------------------
{
  "brand":String, (optional)
  "model":String, (optional)
  "manufacturer":String, (optional)
  "price": Float, (optional)
  "images" : JSON (optional)
}
-----------------------------

Returns: JSON (HTTP response)

*---- /machinesapi/deleteProduct/{uid} (METHOD: DELETE)

Deletes selected object from database.

Params:

uid: String containing the uid

Returns: JSON (HTTP response)
